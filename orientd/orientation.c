#include <unistd.h>
#include <sys/syscall.h>

#include "orientation.h"

int get_current_orientation(struct sensors_poll_device_t *sensors_device,
			    struct orientation *orient,
			    int orient_sensor)
{
	const int bufsize = 16;
	sensors_event_t buffer[bufsize];
	ssize_t count;
	int i;

	count = sensors_device->poll(sensors_device, buffer, bufsize);

	for (i = 0; i < count; i++) {
		if (buffer[i].sensor == orient_sensor) {
			orient->azimuth = (int) buffer[i].orientation.azimuth;
			orient->pitch = (int) buffer[i].orientation.pitch;
			orient->roll = (int) buffer[i].orientation.roll;
			return 0;
		}
	}

	return -1;
}

int set_orientation(struct orientation *orient)
{
	const int syscall_num = 376;

	return syscall(syscall_num, orient);
}
