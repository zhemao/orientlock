#ifndef DEV_ORIENTATION_H
#define DEV_ORIENTATION_H

#include <hardware/sensors.h>

struct orientation {
	int azimuth;
	int pitch;
	int roll;
};

int get_current_orientation(struct sensors_poll_device_t *sensors_device,
			    struct orientation *orient,
			    int orient_sensor);
int set_orientation(struct orientation *orient);

#endif
