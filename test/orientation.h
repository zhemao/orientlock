#ifndef ORIENTATION_H
#define ORIENTATION_H

#include <unistd.h>
#include <sys/syscall.h>

struct dev_orientation {
	int azimuth;
	int pitch;
	int roll;
};

struct orientation_range {
	struct dev_orientation orient;  /* device orientation */
	unsigned int azimuth_range;     /* +/- degrees around Z-axis */
	unsigned int pitch_range;       /* +/- degrees around X-axis */
	unsigned int roll_range;        /* +/- degrees around Y-axis */
};

struct orientation_range facedown = {
	.orient = {
		.azimuth = 0,
		.pitch = 180,
		.roll = 0
	},
	.azimuth_range = 0,
	.pitch_range = 5,
	.roll_range = 5
};

#define orientlock_read(orient) syscall(377, orient)
#define orientlock_write(orient) syscall(378, orient)
#define orientunlock_read(orient) syscall(379, orient)
#define orientunlock_write(orient) syscall(380, orient)

inline void readlock_sighandler(int sig)
{
	orientunlock_read(&facedown);
}

inline void writelock_sighandler(int sig)
{
	orientunlock_write(&facedown);
}

#endif
