static inline int slurp_file(const char *filename, char **ptr)
{
	FILE *f = fopen(filename, "r");
	int len, i;

	if (f == NULL)
		return -1;

	if (fseek(f, 0, SEEK_END) != 0)
		return -1;

	len = ftell(f);

	if (len <= 0)
		return -1;

	if (fseek(f, 0, SEEK_SET) != 0)
		return -1;

	*ptr = malloc(len+1);

	if (fread(*ptr, 1, len, f) != len) {
		free(*ptr);
		return -1;
	}

	ptr[0][len] = '\0';

	for (i = len - 1; i >= 0; i--) {
		if (ptr[0][i] != '\n' && ptr[0][i] != ' ')
			break;
		ptr[0][i] = '\0';
	}

	if (i < 0) {
		free(*ptr);
		return -1;
	}

	return i;
}
