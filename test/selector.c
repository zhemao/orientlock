#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "orientation.h"

int main(int argc, char *argv[])
{
	FILE *f;

	if (argc < 2) {
		printf("Usage: %s number\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if (orientlock_write(&facedown)) {
		perror("error");
		return EXIT_FAILURE;
	}

	signal(SIGINT, writelock_sighandler);

	f = fopen("integer", "w");

	if (f == NULL)
		perror("error");
	else {
		fputs(argv[1], f);

		fclose(f);
	}

	if (orientunlock_write(&facedown)) {
		perror("error");
		return EXIT_FAILURE;
	}

	return 0;
}
