#include <unistd.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <stdlib.h>
#include "orientation.h"

int main(void)
{
	struct orientation_range range;
	char rw;

	printf("r/w? ");
	if (!scanf("%c", &rw))
		goto failure;

	printf("azimuth? ");
	if (!scanf("%d", &range.orient.azimuth))
		goto failure;

	printf("pitch? ");
	if (!scanf("%d", &range.orient.pitch))
		goto failure;

	printf("roll? ");
	if (!scanf("%d", &range.orient.roll))
		goto failure;

	printf("azimuth_r? ");
	if (!scanf("%u", &range.azimuth_range))
		goto failure;

	printf("pitch_r? ");
	if (!scanf("%u", &range.pitch_range))
		goto failure;

	printf("roll_r? ");
	if (!scanf("%u", &range.roll_range))
		goto failure;

	if ('r' == rw) {
		if (orientlock_read(&range))
			goto lock_failure;
	} else if ('w' == rw) {
		if (orientlock_write(&range))
			goto lock_failure;
	} else {
		goto failure;
	}

	printf("We have the lock. q to unlock\n");
	while (getchar() != 'q')
		;

	if ('r' == rw) {
		if (orientunlock_read(&range))
			goto lock_failure;
	} else {
		if (orientunlock_write(&range))
			goto lock_failure;
	}
	exit(EXIT_SUCCESS);

failure:
	fprintf(stderr, "Failed when reading arguments.\n");
	exit(EXIT_FAILURE);

lock_failure:
	fprintf(stderr, "Failed at lock/unlock.\n");
	exit(EXIT_FAILURE);
}

