#ifndef _LINUX_ORIENTATION_H
#define _LINUX_ORIENTATION_H

#include <linux/init.h>
#include <linux/types.h>

struct orientation {
	int azimuth;
	int pitch;
	int roll;
};

struct orientation_range {
	struct orientation orient;
	unsigned int azimuth_range;
	unsigned int pitch_range;
	unsigned int roll_range;
};

struct range_lock {
	struct orientation_range range;
	struct list_head list;
	pid_t pid;
	int lock_held;
	rwlock_t lock_held_lock;
};

#endif
