#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/syscalls.h>
#include <linux/sched.h>
#include <linux/list.h>
#include <linux/uaccess.h>

#include <linux/orientation.h>

static struct orientation current_orient = {0, 0, 0};
static DEFINE_RWLOCK(orient_lock); /* rwlock associated with current_orient */

static LIST_HEAD(read_range_lock_list);
static DEFINE_RWLOCK(read_range_list_lock);
static LIST_HEAD(write_range_lock_list);
static DEFINE_RWLOCK(write_range_list_lock);

static DECLARE_WAIT_QUEUE_HEAD(range_lock_waitq);

static int RANGE_UNLOCKED = 1;
static int RANGE_LOCKED = 2;

/* returns 1 if a and b are less than or equal to gap apart
 * and false if difference between a and b is greater than gap
 * handles degree wraparound */
static int within_range(int a, int b, int gap)
{
	int diff = a - b;

	if (diff < 0)
		diff = -diff;

	diff %= 360;

	if (diff > 180)
		diff = 360 - diff;

	return diff <= gap;
}

/* return 1 if orientation is valid, 0 otherwise */
static int orientation_valid(struct orientation *orient)
{
	if (orient->azimuth < 0 || orient->azimuth >= 360)
		return 0;

	if (orient->pitch < -180 || orient->pitch > 180)
		return 0;

	if (orient->roll < -90 || orient->roll > 90)
		return 0;

	return 1;
}

static int range_valid(struct orientation_range *range)
{
	if (!orientation_valid(&range->orient))
		return 0;

	if (range->azimuth_range < 0)
		return 0;

	if (range->pitch_range < 0)
		return 0;

	if (range->roll_range < 0)
		return 0;

	return 1;
}

/* Returns 1 if ranges are equal, 0 otherwise. */
static int equal_range(struct orientation_range *r1,
		       struct orientation_range *r2)
{
	if (r1->azimuth_range != r2->azimuth_range ||
			r1->pitch_range != r2->pitch_range ||
			r1->roll_range != r2->roll_range ||
			r1->orient.azimuth != r2->orient.azimuth ||
			r1->orient.pitch != r2->orient.pitch ||
			r1->orient.roll != r2->orient.roll)
		return 0;
	return 1;
}

/* Returns 1 if ranges overlap, 0 otherwise. */
static int overlapped_range(struct orientation_range *r1,
			    struct orientation_range *r2)
{
	if (r1->azimuth_range > 0 && r2->azimuth_range > 0 &&
		!within_range(r1->orient.azimuth, r2->orient.azimuth,
			      r1->azimuth_range + r2->azimuth_range))
		return 0;
	if (r1->pitch_range > 0 && r2->pitch_range > 0 &&
		!within_range(r1->orient.pitch, r2->orient.pitch,
			      r1->pitch_range + r2->pitch_range))
		return 0;
	if (r1->roll_range > 0 && r2->roll_range > 0 &&
		!within_range(r1->orient.roll, r2->orient.roll,
			      r1->roll_range + r2->roll_range))
		return 0;
	return 1;
}

/* Returns 1 if orient is within range, 0 otherwise. */
static int is_in_range(struct orientation *orient,
		       struct orientation_range *range)
{
	if (range->azimuth_range > 0 &&
		!within_range(orient->azimuth,
			      range->orient.azimuth,
			      range->azimuth_range))
		return 0;
	if (range->pitch_range > 0 &&
		!within_range(orient->pitch,
			      range->orient.pitch,
			      range->pitch_range))
		return 0;
	if (range->roll_range > 0 &&
		!within_range(orient->roll,
			      range->orient.roll,
			      range->roll_range))
		return 0;

	return 1;
}

/* Returns 1 if the range_lock was found and removed, 0 otherwise.
 * Important! write lock the list before calling this */
static int remove_range_lock(struct range_lock *krlock,
			     struct list_head *list)
{
	struct range_lock *rlock;
	struct range_lock *temp;

	list_for_each_entry_safe(rlock, temp, list, list) {
		if (equal_range(&krlock->range, &rlock->range)
			&& krlock->pid == rlock->pid) {
			pr_info("Deleting lock.\n");
			list_del(&rlock->list);
			kfree(rlock);
			return 1;
		}
	}
	return 0;
}

/* returns 1 if something in the list contains this orientation and is locked
 * Important! read lock the list before calling this */
static int is_locked(struct orientation *orient, struct list_head *list)
{
	struct range_lock *rlock;

	list_for_each_entry(rlock, list, list) {
		if (is_in_range(orient, &rlock->range) && rlock->lock_held)
			return 1;
	}

	return 0;
}

/* returns true if lock_held is false and LSB of mask is set
 * or if lock_held is true and second LSB of mask is set */
#define lock_fits_mask(lock_held, mask) ((mask) & (1<<(lock_held)))


/* returns 1 if range locks in the list matching lock_mask overlap
 * with the range in rlock and 0 otherwise */
static int range_contended(struct range_lock *rlock,
			   struct list_head *list,
			   int lock_mask)
{
	struct range_lock *lrlock;

	list_for_each_entry(lrlock, list, list) {
		/* don't want to compare rlock to itself */
		if (lrlock == rlock)
			continue;

		read_lock(&lrlock->lock_held_lock);

		if (lock_fits_mask(lrlock->lock_held, lock_mask) &&
			overlapped_range(&rlock->range, &lrlock->range)) {

			read_unlock(&lrlock->lock_held_lock);
			return 1;
		}
		read_unlock(&lrlock->lock_held_lock);
	}

	return 0;
}

/* assigns any write locks pending on the given orientation
 * returns 1 if write lock assigned and 0 otherwise */
static int assign_write_lock(struct orientation *orient)
{
	struct range_lock *rlock;

	list_for_each_entry(rlock, &write_range_lock_list, list) {
		if (is_in_range(orient, &rlock->range) &&
			!range_contended(rlock, &write_range_lock_list,
					 RANGE_LOCKED) &&
			!range_contended(rlock, &read_range_lock_list,
					 RANGE_LOCKED)) {

			write_lock(&rlock->lock_held_lock);
			rlock->lock_held = 1;
			write_unlock(&rlock->lock_held_lock);
			pr_info("Gave lock to %d\n", rlock->pid);
			return 1;
		}
	}

	return 0;
}

/* assigns any read locks pending on the given orientation
 * returns number of read locks assigned */
static int assign_read_locks(struct orientation *orient)
{
	struct range_lock *rlock;
	int locks_given = 0;

	list_for_each_entry(rlock, &read_range_lock_list, list) {
		if (is_in_range(orient, &rlock->range) &&
			!range_contended(rlock, &write_range_lock_list,
					 RANGE_LOCKED | RANGE_UNLOCKED)) {

			write_lock(&rlock->lock_held_lock);
			rlock->lock_held = 1;
			locks_given++;
			write_unlock(&rlock->lock_held_lock);
			pr_info("Gave lock to %d\n", rlock->pid);
		}
	}

	return locks_given;
}


/* assigns any pending read or write locks pending on given orientation
 * returns number of locks assigned */
static int assign_locks(struct orientation *orient)
{
	int locks_given = 0;

	read_lock(&write_range_list_lock);

	/* if there is a write lock, we can do nothing */
	if (is_locked(orient, &write_range_lock_list)) {
		pr_info("Write locked, can't do anything.\n");
		read_unlock(&write_range_list_lock);
		return 0;
	}

	read_lock(&read_range_list_lock);

	/* if no read lock, we can assign a write lock */
	if (!is_locked(orient, &read_range_lock_list)) {
		if (assign_write_lock(orient)) {
			read_unlock(&read_range_list_lock);
			read_unlock(&write_range_list_lock);
			return 1;
		}
	}

	/* if no write locks were assigned, we can assign read locks
	 * as long as no write locks are pending */

	locks_given = assign_read_locks(orient);

	read_unlock(&read_range_list_lock);
	read_unlock(&write_range_list_lock);

	return locks_given;
}

SYSCALL_DEFINE1(set_orientation, struct orientation __user *, orient)
{
	struct orientation korient;
	int locks_given;

	if (current->cred->uid != 0)
		return -EACCES;

	if (orient == NULL)
		return -EINVAL;

	if (!access_ok(VERIFY_READ, orient, sizeof(struct orientation)))
		return -EFAULT;

	/* Copy to local orientation variable */
	if (copy_from_user(&korient, orient, sizeof(struct orientation)))
		return -EFAULT;

	if (!orientation_valid(&korient)) {
		pr_info("DEBUG: invalid orientation %d %d %d\n",
			korient.azimuth, korient.pitch, korient.roll);
		return -EINVAL;
	}

	write_lock(&orient_lock);

	current_orient.azimuth = korient.azimuth;
	current_orient.pitch = korient.pitch;
	current_orient.roll = korient.roll;

	write_unlock(&orient_lock);

	read_lock(&orient_lock);

	locks_given = assign_locks(&current_orient);

	read_unlock(&orient_lock);

	wake_up(&range_lock_waitq);

	return locks_given;
}

/* waits in the wait queue until rlock->lock_held is 1 */
static void wait_for_rlock(struct range_lock *rlock)
{
	DEFINE_WAIT(wait);

	add_wait_queue(&range_lock_waitq, &wait);

	pr_info("Waiting for lock.\n");

	while (1) {
		read_lock(&rlock->lock_held_lock);

		if (rlock->lock_held) {
			read_unlock(&rlock->lock_held_lock);
			break;
		}

		read_unlock(&rlock->lock_held_lock);

		prepare_to_wait(&range_lock_waitq, &wait, TASK_INTERRUPTIBLE);

		schedule();
	}

	finish_wait(&range_lock_waitq, &wait);

	pr_info("Acquired lock.\n");
}

SYSCALL_DEFINE1(orientlock_read, struct orientation_range __user *, orient)
{
	struct range_lock *krlock;

	if (!orient)
		return -EINVAL;

	if (!access_ok(VERIFY_READ, orient, sizeof(struct orientation_range)))
		return -EFAULT;

	krlock = kmalloc(sizeof(struct range_lock), GFP_KERNEL);
	if (!krlock)
		return -ENOMEM;

	if (copy_from_user(&krlock->range, orient,
				sizeof(struct orientation_range)))
		return -EFAULT;

	if (!range_valid(&krlock->range)) {
		kfree(krlock);
		return -EINVAL;
	}

	pr_info("Locking %d +/- %d, %d +/- %d, %d +/- %d\n",
		krlock->range.orient.azimuth, krlock->range.azimuth_range,
		krlock->range.orient.pitch, krlock->range.pitch_range,
		krlock->range.orient.roll, krlock->range.roll_range);

	krlock->pid = current->pid;
	krlock->lock_held = 0;
	rwlock_init(&krlock->lock_held_lock);

	write_lock(&read_range_list_lock);

	list_add_tail(&krlock->list, &read_range_lock_list);

	write_unlock(&read_range_list_lock);

	read_lock(&orient_lock);

	assign_locks(&current_orient);

	read_unlock(&orient_lock);

	wait_for_rlock(krlock);

	return 0;
}

SYSCALL_DEFINE1(orientlock_write, struct orientation_range __user *, orient)
{
	struct range_lock *krlock;

	if (!orient)
		return -EINVAL;

	if (!access_ok(VERIFY_READ, orient, sizeof(struct orientation_range)))
		return -EFAULT;

	krlock = kmalloc(sizeof(struct range_lock), GFP_KERNEL);
	if (!krlock)
		return -ENOMEM;

	if (copy_from_user(&krlock->range, orient,
				sizeof(struct orientation_range)))
		return -EFAULT;

	if (!range_valid(&krlock->range)) {
		kfree(krlock);
		return -EINVAL;
	}

	pr_info("Locking %d +/- %d, %d +/- %d, %d +/- %d\n",
		krlock->range.orient.azimuth, krlock->range.azimuth_range,
		krlock->range.orient.pitch, krlock->range.pitch_range,
		krlock->range.orient.roll, krlock->range.roll_range);

	krlock->pid = current->pid;
	krlock->lock_held = 0;
	rwlock_init(&krlock->lock_held_lock);

	write_lock(&write_range_list_lock);

	list_add_tail(&krlock->list, &write_range_lock_list);

	write_unlock(&write_range_list_lock);

	read_lock(&orient_lock);

	assign_locks(&current_orient);

	read_unlock(&orient_lock);

	wait_for_rlock(krlock);

	return 0;
}

SYSCALL_DEFINE1(orientunlock_read, struct orientation_range __user *, orient)
{
	struct range_lock krlock;
	int retval = 0;

	if (!orient)
		return -EINVAL;

	if (!access_ok(VERIFY_READ, orient, sizeof(struct orientation_range)))
		return -EFAULT;

	if (copy_from_user(&krlock.range, orient,
				sizeof(struct orientation_range)))
		return -EFAULT;

	krlock.pid = current->pid;

	write_lock(&read_range_list_lock);

	if (!remove_range_lock(&krlock, &read_range_lock_list)) {
		pr_info("Could not remove lock for pid %d.\n", krlock.pid);
		retval = -EINVAL;
	}

	write_unlock(&read_range_list_lock);

	read_lock(&orient_lock);

	assign_locks(&current_orient);

	read_unlock(&orient_lock);

	wake_up(&range_lock_waitq);

	return retval;
}

SYSCALL_DEFINE1(orientunlock_write, struct orientation_range __user *, orient)
{
	struct range_lock krlock;
	int retval = 0;

	if (!orient)
		return -EINVAL;

	if (!access_ok(VERIFY_READ, orient, sizeof(struct orientation_range)))
		return -EFAULT;

	if (copy_from_user(&krlock.range, orient,
				sizeof(struct orientation_range)))
		return -EFAULT;

	krlock.pid = current->pid;

	write_lock(&write_range_list_lock);

	if (!remove_range_lock(&krlock, &write_range_lock_list)) {
		pr_info("Could not find lock for pid %d.\n", krlock.pid);
		retval = -EINVAL;
	}

	write_unlock(&write_range_list_lock);

	read_lock(&orient_lock);

	assign_locks(&current_orient);

	read_unlock(&orient_lock);

	wake_up(&range_lock_waitq);

	return retval;
}
